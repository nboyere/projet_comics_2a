<?php
	class Modele_comics extends CI_Model
	{
		
		public function __construct()
        {
            $this->load->database();
        }
        //select('select * from comics._comic except select comic_id,serie,numero,date,couverture from comics.ViewCollection where collector_login =\''.$id.'\';')
       
        public function getUserComics($id){
			return $this->db->select('*')->from('comics.viewcollection')->where('collector_login', $id)->get()->result_array();
		}
		
		public function getOtherComics($id,$n){
			
				$n = "offset ".$n*10;
			
			return $this->db->query('select * from comics._comic except select comic_id,serie,numero,date,couverture from comics.ViewCollection where collector_login =\''.$id.'\' order by comic_id limit 10'.$n.';')->result_array();
		}
		
		public function supprimerComic($id, $idcomic){
			$this->db->where('comic_id', $idcomic, 'collector_login', $id);
			$this->db->delete('comics._collect');
		}
		
		public function ajouterComic($id, $idcomic){
			$data = array(	'collector_login' => $id,
							'comic_id' => $idcomic);
			$this->db->insert('comics.viewcollection',$data);
		}
		
		public function viderComics($id){
			$this->db->where('collector_login', $id);
			$this->db->delete('comics._collect');
		}
		
}?>
