<?php
	class Modele_connexion extends CI_Model
	{
		
		public function __construct()
        {
            $this->load->database();
        }
        
        public function login($identifiant, $password)
        {
            $data = $this->db->query('select * from comics._collector where login = \''.$identifiant.'\' AND password = \''.$password.'\';')->result_array();
            //je n'ai pas pu utiliser le query builder de code igniter car avec le login 'admin' il ne prennais pas en compte le mot de passe alors qu'avec un autre login si.
            return (count($data) === 1);
        }
        
        public function getUserData($identifiant, $password){
			return $this->db->select('*')->from('comics._collector')->where('login', $identifiant, 'password', $password)->get()->result_array();
		}

		public function UserExists($identifiant)
        {
            $data = $this->db->select('*')->from('comics._collector')->where('login', $identifiant)->get()->result_array();

            return (count($data) === 1);
        }
        
        public function addUser($post){
			$data = array(
				'login' => $post["identifiant"],
				'name' => $post["nom"],
				'firstname' => $post["prenom"],
				'password' => $post["password"],
				'admin' => 0
			);
			
			$this->db->insert('comics._collector', $data);
		}
		
		public function deleteUser($id){
			$this->db->delete('comics._collect', array('collector_login' => $id));

			$this->db->delete('comics._collector', array('login' => $id));
		}
		
		public function setAdmin($id,$n){
				$this->db->where('login', $id);
				$this->db->update('comics._collector', array("admin" => $n));
			
			
		}
		
		public function isAdmin($id){
			$data = $this->db->select('*')->from('comics._collector')->where('login', $id)->get()->result_array();
            return ($data[0]['admin'] == true);
		}
		
		public function userList(){
			return $this->db->select('login, name, firstname, admin')->from('comics._collector')->order_by('login')->get()->result_array();	
		}
}
?>
