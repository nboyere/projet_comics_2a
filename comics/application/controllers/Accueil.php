<?php
defined('BASEPATH') OR exit('No direct script access allowed');




class Accueil extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
			$this->load->library('session');
			$this->load->model('modele_connexion');
			$this->load->model('modele_comics');
			$this->load->helper('url');
			
}

	public function index() {
		if($this->session->logged === true){
			$data["comics"] = $this->modele_comics->getUserComics($this->session->login);

			$this->load->view('AccueilUtil', $data);
		}else{
			session_destroy();
			$this->load->view('Accueil');
		}
		
	
	}


	public function connexion() {
			$identifiant = $this->input->post('identifiant');
			$password = $this->input->post('password');
			$resultat = $this->modele_connexion->login($identifiant,$password);
			if($resultat){
				$info = $this->modele_connexion->getUserData($identifiant,$password);
				
				$data["login"] = $info[0]['login'];
				$data["name"] = $info[0]['name'];
				$data["firstname"] = $info[0]['firstname'];
				$data["logged"] = true;
				$data["isadmin"] = $this->modele_connexion->isAdmin($info[0]['login']);
           		$this->session->set_userdata($data);
           		redirect(base_url());
			}else{
				$this->session->set_userdata("infoInscription","Identifiant / Mot de passe incorrect !");
				redirect(base_url());
			}
			
		}
	public function inscription() {
		
			$post = $this->input->post();
			$existe = $this->modele_connexion->UserExists($post["identifiant"]);
			if($existe){
				$this->session->set_userdata("infoInscription","Le login est déjà utilisé !");
			}else if($post["password"] != $post["password2"]){
				$this->session->set_userdata("infoInscription","Les mots de passes ne sont pas identiques !");
			}else{
				
				$this->session->set_userdata("infoInscription","Compte créé avec succès !");
				$this->modele_connexion->addUser($post);
			}
			$this->load->view('Accueil');
		}
		
	public function deconnexion() {
			$this->session->set_userdata("logged", false);
			redirect(base_url());
		}

	public function admin(){
		if($this->modele_connexion->isAdmin($this->session->login)){
			$data['users'] = $this->modele_connexion->userList();
			$this->load->view('GestionUtil',$data);
		}else{
			show_error('Accès interdit pour les utilisateurs non administrateurs',401);
		}
	}

	public function supprimerUser() {
		$get = $this->input->get();
		$this->modele_connexion->deleteUser($get['login']);
		redirect(base_url('/index.php/Accueil/admin'));
	}
	
	public function switchAdmin() {
		$get = $this->input->get();
		if($get['admin'] == 1){
			$n = 0;
		}else{
			$n = 1;
		}

		$this->modele_connexion->setAdmin($get['login'],$n);
		redirect(base_url('/index.php/Accueil/admin'));
	}

	public function comics(){
		$n = $this->input->get("page");

		if(!isset($n)){$n = 0;}
		$data["comics"] = $this->modele_comics->getOtherComics($this->session->login,$n);
		$data["npage"] = $n;	
		$this->load->view('comics',$data);
	}

	public function supprimer() {
			$comicid = $this->input->get('id');
			$this->modele_comics->supprimerComic($this->session->login, $comicid);
			redirect(base_url());
	}
	
	public function ajouter() {
			$comicid = $this->input->get('id');
			$this->modele_comics->ajouterComic($this->session->login, $comicid);
			redirect(base_url('/index.php/Accueil/comics'));
	}
	
	
	


}
?>
