<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="fr">

<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css');?>">
<head>
    <meta charset="utf-8">
    <title>Accueil</title>
</head>

<body>

    <?php $this->load->view('nav'); ?>

    <div id="container" class="text-center">
        <h1>Bienvenue <?php echo $this->session->firstname . " " .$this->session->name ?></h1>
        <?php 
            if(empty($comics)){
                echo "<p>Ta collection est vide! Clique sur l'onglet 'Liste comics' pour ajouter des comics</p>";
            }
        ?>
        
    </div>
    <div class="container">
        <div class="row">
                
             <?php 
            if(!empty($comics)){    
                foreach($comics as $row){
                    echo '<div class="card m-3" style="font-size:14px; width: 10rem;">
                          <img class="card-img-top" src="'.$row["couverture"].'">
                          <div class="num">'.$row["numero"].'</div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item text-center">'.$row["serie"].'</li>
                            <li class="list-group-item text-center">Parution: '.$row["date"].'</li>
                            
                          </ul>
                          <div class="card-body text-center">
                            <a href="'.base_url('/index.php/Accueil/supprimer?id=').$row["comic_id"].'" class="toggle toggleBlue">Supprimer</a>
                          </div>
                        </div>';
            	}
            }
             ?>
        

        <div>         
    </div>

</body>

</html>
