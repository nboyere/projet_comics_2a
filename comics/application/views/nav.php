<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  
  <?php if($this->session->isadmin){
            echo '<a class="navbar-brand" style="font-size: 15px; color:#ff8624;">'.$this->session->login;
        }else{
            echo '<a class="navbar-brand" style="font-size: 15px; color:#32a852;">'.$this->session->login;   
        } ?>
    </a>
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>">Ma collection</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('/index.php/Accueil/comics'); ?>">Liste comics</a>
      </li>
      <?php if($this->session->isadmin){ 
        echo '<li class="nav-item">
		        <a class="nav-link" href="'.base_url('index.php/Accueil/admin').'" >Gestion membres</a>
		      </li>';
  		}
      ?>
      </ul>
      <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" id="deconnexion" href="<?php echo base_url('/index.php/accueil/deconnexion'); ?>">Deconnexion</a>
      </li>
     </ul>
    
</nav>