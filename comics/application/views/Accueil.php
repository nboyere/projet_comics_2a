<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="fr">

<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css');?>">
<script src="<?php echo base_url('assets/js/jquery/jquery-1.11.1.js');?>"></script>
<head>
    <meta charset="utf-8">
    <title>Connexion</title>
</head>



<body>
	<div class="login-page">
		<div class="form">
			<?php $this->load->view('inscription') ?>
			<?php $this->load->view('connexion') ?>
		</div>
	
		<a href="<?php echo base_url('index.php/accueil/comics'); ?>"><div class="text-center btnAccueil">
			Liste des comics disponibles
		</div></a>	
	</div>

</body>
<script>
	$('.message a').click(function(){
	   $('form').animate({height: "toggle", opacity: "toggle"}, "medium");
	});
</script>

</html>
