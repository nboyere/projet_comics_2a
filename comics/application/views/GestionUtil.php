<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="fr">
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css');?>">


<head>
    <meta charset="utf-8">
    <title>Gestion membres</title>
</head>



<body>

    <?php $this->load->view('nav'); ?>

    <div class="row m-1" >
   
        <?php foreach($users as $row){
            if($row['admin'] == 1){
                    $checked = 'checked';
                }else{
                    $checked = '';
                }
                if($row['login'] != $this->session->login){
                    echo '<div class="col-2 my-2 ">
                                  <div class="">
                                    <div class="card">
                                      <div class="card-body">';
                                    echo '<h5 class="card-title">'.$row['login'].'</h5>';
                                    echo  '<p class="card-text">Prénom : '.$row['firstname'].'<br>Nom : '.$row['name'].'</p>';
                                    echo '<a href="'.base_url().'index.php/Accueil/switchAdmin?login='.$row['login'].'&admin='.$row['admin'].'">';
                                echo '<div class=" toggle ">admin<input class="readmenot" '.$checked.' type="checkbox"></input></div></a>';
                                echo '<a href="'.base_url().'index.php/Accueil/supprimerUser?login='.$row['login'].'">';       
                                echo '<div class="suppUser toggle">Bannir</div></a>
                                      </div>
                                    </div>
                                  </div>
                                </div>';

                    }
                }
            ?>

    </div>

</body>

</html>